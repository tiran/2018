---
abstract: Crypto is used for a lot more than just currencies. This talk will dive
  into modern cryptography, the math behind how it works, and its everyday use cases.
  By looking at the origins of cryptography we'll follow the progression of methods
  and algorithms as humans and computers evolved.
duration: 45
level: All
presentation_url: https://www.slideshare.net/KelleyRobinson1/practical-cryptography-118326416
room: PennTop North
slot: 2018-10-05 11:15:00-04:00
speakers:
- Kelley Robinson
title: Practical Cryptography
type: talk
video_url: https://youtu.be/SzzeZz_NPf8
---

From TLS to authentication, "crypto" is used for a lot more than just currencies. In 2018 security should be part of every engineer's toolkit and cryptography is a foundation we can master together. This talk will dive into modern cryptography, the math behind how it works, and its everyday use cases. By looking at the origins of cryptography we'll follow the progression of methods and algorithms as humans and computers evolved. 

You'll leave understanding the difference between symmetric and asymmetric cryptography, why you would have a public and private key, and how those get used in a variety of applications. We'll look at how to encrypt and decrypt data using Python's `cryptography` library and discuss the reasons you should never roll your own crypto. This will *not* be a talk about bitcoin, but will dive into how cryptography helps secure anonymous transactions and keeps your identity and data safe.
