---
title: Young Coders
---

Ever wonder how web sites and games are made? If you are 10 years old or over,
join us for a few hours on Sunday, October 7th to explore how to program, and
why programmers love Python so much.

Python is a very powerful programming language: it is used for writing computer
applications, making websites and games, scientific and mathematical computing,
and much more. Some of the companies that use Python include: Google, NASA,
Disney Animation, and Spotify.

This free tutorial will explore how to program using Python starting with the
basics: math and words, comparisons, loops, and kinds of data. Finally, we will
combine our new knowledge by using our skills to do some fun robotics projects
with the BBC micro:bit.

We'll provide computers with everything you need already installed, and every
student will get to take home their own micro:bit.

Registration for this event is open! Students must be registered by a parent
or guardian, and each student must bring a release form signed by a parent or
guardian to be allowed into the class. A link to that form is provided at the
registration page. (If you are interested in helping out as a teacher's
assistant, you'll be able to sign up at the same registration page.)

Register here: <https://pygotham2018youngcoders.eventbrite.com>

## About the instructor

Lillie Schachter is a Software Engineer and Educator based in Brooklyn. A
Liberal Arts education centered around a Computer Science major led Lillie to
wonder how we can use code to better the world. Empowering students with the
skills, excitement, and awareness to utilize responsible programming continues
to be one answer to this question. Building tools to help solve problems is
another. Lillie is very excited to join the PyGotham team and Young Coders
event!
