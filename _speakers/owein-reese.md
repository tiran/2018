---
name: Owein Reese
talks:
- Pushing the Bounds with Descriptors
---

Owein is the Director of Creatives Engineering at MediaMath. He with his teams have built distributed systems which handle over 1M req/s with requirements of under 10ms latency daily at the 99.9%. Their software runs on every thing from AWS Lambdas and Containers to bare metal boxes in every continent except Antarctica. 

He, himself, began a career after grad school as a mathematical programmer working in infrared countermeasures and anti-missile technology. After a few years in defense he moved to work on NASA satellite systems followed by a multi-year foray into the hedge fund world with a brief stint at an investment bank. Finally he wound up in adtech, building high throughput, distributed systems.